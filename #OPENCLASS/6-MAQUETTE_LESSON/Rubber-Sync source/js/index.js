//================================SCROLLSPY

$(function () {
      $('header a').on('click', function(e) {
        e.preventDefault();
        var hash = this.hash;
        $('html, body').animate({
          scrollTop: $(this.hash).offset().top
        }, 1000, function(){
          window.location.hash = hash;
        });
      });
    });

//================================ASIDE
const setAside = () =>  {
  var afdoc = $('#affixMe');
  var afpos = afdoc.position();
  var offToped,
      offBotted,
      asideH,
      botAbso;

  offToped  = ($('nav').height() + $('.rub_banner').height() + $('.rub_downinfo').height()) + 80;
  offBotted = (($('.sec_head').height() * 3) + ($('.sec_body').height() * 3)) - 55 ;
  asideH    = $('.rub_aside').height();
  botAbso   = ($(".rub_sec").height() - ($('.rub_aside').height() + 160));

  $('.myAffix').css({'top':offToped +'px'});

  $(window).scroll( (event) => {
      var scroll = $(window).scrollTop();

      if (scroll > (offBotted - 55))
      {
       $('.myAffix').css({'position': 'absolute'});
       $('.myAffix').css({'top':botAbso + 'px'});
      }
      else if (scroll > offToped && scroll < offBotted)
      {
        if (!($('.myAffix').css("position") === "static"))
        {
          $('.myAffix').css({'position': 'fixed'});
          $('.myAffix').css({'top':50+'px'});
        }
      }
      else {
        if (scroll < (offBotted+150))
        {
          if ($('.myAffix').css("position") === "fixed")
          {
            $('.myAffix').css({'position': 'absolute'});
            $('.myAffix').css({'top':offToped+'px'});
          }
        }
      }
  });
};



//================================CLICK EVENT FUNCTION

$('.a0006').on('click', () => {
  $('.a0006').addClass('active');
});

$('.downLink').on('click', () => {
  setTimeout( () => {
    window.location.href = "https://github.com/MonuMonuMonu/Rubber-Sync/releases/";
  }, 1500);
});

//================================POST MAIL

   var form_id = "jquery_form";

   var data = {
       "access_token": "cn4majqm6hl4mew67j4906rx"
   };

   function onSuccess () {
       window.location = window.location.pathname + "?message=Email+Successfully+Sent%21&isError=0";
   };

   function onError (error) {
       window.location = window.location.pathname + "?message=Email+could+not+be+sent.&isError=1";
   };

   var sendButton = $(".c0002");

   function send () {
       sendButton.val('Sending…');
       sendButton.prop('disabled',true);

       var subject = $("#" + form_id + " [name='subject']").val();
       var message = $("#" + form_id + " [name='text']").val();
       data['subject'] = subject;
       data['text'] = message;

       $.post('https://postmail.invotes.com/send',
           data,
           onSuccess
       ).fail(onError);

       return false;
   }

   sendButton.on('click', send); //sendButton.on('click', submit);

   var $form = $("#" + form_id);
   $form.submit(function( event ) {
       event.preventDefault();
   });

//================================REFRESH
   $(window).resize( function () {
     setAside();
   });

//================================INIT ASIDE
setAside();
