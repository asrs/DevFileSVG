/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               GLOBAL INIT                                 */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const flur  = console.log;
const mapHTML   = document.getElementById('map');
const but_attack = document.getElementById('but_attack');
const but_move = document.getElementById('but_move');
const but_defend = document.getElementById('but_defend');
var isoInitTop,
    isoInitLeft,
    mapWidth,
    mapHeight;
var isMap = [];
var but_actived = false;

var playerOne = {
  name: "playerOne",
  vie : 100,
  pos: 0,
  arme: "Main de fer",
  degat: 10,
  defend: 0,
  tour: 0,
  moveRange: 9,
  attackRange: 9,
};
var playerTwo = {
  name: "playerTwo",
  vie : 100,
  Pos: 0,
  arme: "Main de fer",
  degat: 10,
  defend: 0,
  tour: 0,
  moveRange: 3,
  attackRange: 1,
};
var game = {
  turnNumber: 0,
  count: 1,
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                                 BUTTON                                    */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
//- - -  Attack Button
but_attack.addEventListener('click', () => {
  if (but_actived === false)
  {
    but_actived = true;

    if (game.turnNumber === 1)
    {
      attackPartOne(playerOne, playerTwo);
    };
    if (game.turnNumber === 2)
    {
      attackPartOne(playerTwo, playerOne);
    };
  }
  else {
    but_actived = false;
    eventCanceller();
  };
});

//- - -  Attack Button
but_move.addEventListener('click', () => {
  if (but_actived === false)
  {
    but_actived = true;
    if (game.turnNumber === 1)
      movePartOne(playerOne);
    if (game.turnNumber === 2)
      movePartOne(playerTwo);
  }
  else {
    but_actived = false;
    eventCanceller();
  };
});

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             GET MAP Function                              */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
//- - - Get Map Status
const getMap = () => {
  let iX = 0, iY = 0;

  isMap = [];
  while(iY < 10)
  {
    iX = 0;
    while(iX < 10)
    {
      isMap.push(mapHTML.rows[iY].cells[iX]);
      iX++;
    };
    iY++;
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                            GET PLAYER POSITION                            */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
//- - - Return Player Pos
const getPos = (player) => {
  isMap.forEach((e, i)=> {
    if (e.hasChildNodes())
    {
      if (e.childNodes[0].id === player.name)
        return player.pos = i;
    };
  });
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             GET CLASS Function                            */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
//- - - hasClass function
const hasClass = (el, cls) => {
  return (' ' + el.className + ' ').indexOf(' ' + cls + ' ') > -1;
};
/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               GET CELLS                                   */
/*                                                                           */
/* - - - Collect cells from a range in array before activation of event      */
/* ************************************************************************* */
const getCellsTop = (player, range, yAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos - i) > (player.pos - (range + 1)))
  {
    if (!(typeof isMap[player.pos - (i * 10)] === 'undefined'))
    {
      if (hasClass(isMap[player.pos - (i * 10)], 'rocked') ||
         (stop === true && hasClass(isMap[player.pos - (i * 10)], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos - range) > yAxis)
      {
        actCells.splice(0, 0, isMap[player.pos - (i * 10)]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

//*****************************************************************************
const getCellsDown = (player, range, yAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos + i) < (player.pos + (range + 1)))
  {
    if (!(typeof isMap[player.pos + (i * 10)] === 'undefined'))
    {
      if (hasClass(isMap[player.pos + (i * 10)], 'rocked') ||
         (stop === true && hasClass(isMap[player.pos + (i * 10)], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos + range) < (yAxis + 90))
      {
        actCells.splice(0, 0, isMap[player.pos + (i * 10)]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

//*****************************************************************************
const getCellsLeft = (player, range, xAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos - i) > (player.pos - (range + 1)))
  {
    if (!(typeof isMap[player.pos - i] === 'undefined'))
    {
      if (hasClass(isMap[player.pos - i], 'rocked') ||
         (stop === true && hasClass(isMap[player.pos - i], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos - i) > (xAxis - 1))
      {
        actCells.splice(0, 0, isMap[player.pos - i]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

//*****************************************************************************
const getCellsRight = (player, range, xAxis, actCells, stop) => {
  let i;

  i = 1;
  while ((player.pos + i) < (player.pos + (range + 1)))
  {
    if (!(typeof isMap[player.pos + i] === 'undefined'))
    {
      if (hasClass(isMap[player.pos + i], 'rocked') ||
         (stop === true && hasClass(isMap[player.pos + i], 'Player')))
      {
        i = player.pos + (range + 1);
      }
      else if ((player.pos + i) < (xAxis + 10))
      {
        actCells.splice(0, 0, isMap[player.pos + i]);
        i++;
      }
      else {
        i++;
      };
    }
    else {
      i = player.pos + (range + 1);
    };
  };
  return (actCells);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             ATTACK Function                               */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const attackPartOne = (player, victim) => {
  var range = player.attackRange;
  var xAxis = Math.floor(player.pos/10)*10;
  var yAxis = Number((player.pos + 10).toString()[1]);
  var actCells = [];

  getCellsTop(player, range, yAxis, actCells, false);
  getCellsDown(player, range, yAxis, actCells, false);
  getCellsLeft(player, range, xAxis, actCells, false);
  getCellsRight(player, range, xAxis, actCells, false);

  isMap.forEach((e, i) => {
    if (!(typeof actCells[i] === 'undefined'))
    {
      if (!(hasClass(actCells[i], 'rocked')))
      {
        actCells[i].classList.add('attack');
        actCells[i].onclick = (e) => attackPartTwo(player, victim, e.target);
      };
    };
  });
  return;
};

const attackPartTwo = (player, victim, clickPos) => {
  if (clickPos.id === victim.name)
  {
    if (victim.defend === 0)
    {
      victim.vie -= player.degat;
    }
    else {
      victim.vie -= (player.degat/2);
    };
    //notif of flur
    flur("A T T A C K !");
    flur(victim.name+" : "+victim.vie+' Pv');
    // notif end of turn ++ change turn
    return eventCanceller();
  }
  else {
    //notif of flur
    flur('attack missed...')
    // notif end of turn ++ change turn
    return eventCanceller();
  };
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             MOVE Function                                 */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const movePartOne = (player) => {
  var range = player.moveRange;
  var xAxis = Math.floor(player.pos/10)*10;
  var yAxis = Number((player.pos + 10).toString()[1]);
  var actCells = [];

  getCellsTop(player, range, yAxis, actCells, true);
  getCellsDown(player, range, yAxis, actCells, true);
  getCellsLeft(player, range, xAxis, actCells, true);
  getCellsRight(player, range, xAxis, actCells, true);

  isMap.forEach((e, i) => {
    if (!(typeof actCells[i] === 'undefined'))
    {
      if (!(hasClass(actCells[i], 'rocked')))
      {
        actCells[i].classList.add('move');
        actCells[i].onclick = (e) => movePartTwo(player, e.target);
      };
    };
  });
  return;
};

const movePartTwo = (player, clickPos) => {
   if (hasClass(clickPos, 'weapon'))
   {
     changeWeapon();
   }
  // notif end of turn ++ change turn
  removePlayer(player);
  getMap();
  posPlayer(player, clickPos);
  eventCanceller();
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                         EVENT CANCELLER Function                          */
/*                                                                           */
/* - - - Cancel Move and Attack Cells activation                             */
/* ************************************************************************* */
const eventCanceller = () => {
  isMap.forEach((e, i) => {
    if (!(hasClass(e,'rocked')))
      {
        if (hasClass(e,'attack'))
        {
          e.onclick = "null";
          e.classList.remove('attack');
        };

        if (hasClass(e,'move'))
        {
          e.onclick = "null";
          e.classList.remove('move');
        };
      };
  });
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               INIT ROCK                                   */
/*                                                                           */
/* - - - Initiate rock position                                              */
/* ************************************************************************* */
const setRock = () => {
  var randomNumber = Math.floor((Math.random() * 18) + 2)
  let i = 0;
  var rockPos;

  while (i < randomNumber)
  {
    rockPos = Math.floor(Math.random() * 99);
    if (hasClass(isMap[rockPos], 'rocked'))
    {
      rockPos = Math.floor(Math.random() * 99);
      i--;
    }
    else {
     isMap[rockPos].classList.add('rocked');
    };
    i++;
  };
  return;
};
/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               INIT WEAPON                                 */
/*                                                                           */
/* - - - Initiate weapon position                                            */
/* ************************************************************************* */
const setWeapon = () => {
  let i = 0;
  var weapPos;

  while (i < 4)
  {
    weapPos = Math.floor(Math.random() * 99);
    if (hasClass(isMap[weapPos], 'rocked'))
    {
      weapPos = Math.floor(Math.random() * 99);
      i--;
    }
    else {
     isMap[weapPos].classList.add('weapon');
    };
    i++;
  };
  return setTypeWeapon();
};

const setTypeWeapon = () => {
  var weapons = [];
  let i= 0;

  isMap.forEach((e, i) => {
    if (hasClass(e, 'weapon'))
    {
      weapons.splice(0, 0, i);
    }
  });

  while (i < 4)
  {
    isMap[weapons[i]].classList.add(`Weap${i+1}`);
    flur(isMap[weapons[i]])
    i++;
  }
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               Change WEAPON                               */
/*                                                                           */
/* - - - Change weapon value when player walk on it                          */
/* ************************************************************************* */
const changeWeapon = () => {
flur("Weapon changed");
/*
if (hasClass(isMap[e.target],"weap1"))
  player.arme = "New weap"

  do it 4 time
*/
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                          SET PLAYER Function                              */
/*                                                                           */
/* - - - Initiate player position                                            */
/* ************************************************************************* */
const setPlayer = (player) => {
  var randomNumber = Math.floor(Math.random() * 99);
  var playerElement = document.createElement('div');
  playerElement.id = `${player.name}`;

  if (hasClass(isMap[randomNumber], 'rocked') ||
      hasClass(isMap[randomNumber], 'weapon'))
  {
    setPlayer(player);
  }
  else {
    isMap[randomNumber].appendChild(playerElement);
    isMap[randomNumber].classList.add('Player');
    getMap();
    getPos(player);
  };
  return;
};


/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        REMOVE PLAYER Function                             */
/*                                                                           */
/* - - - REMOVE A PLAYER                                                     */
/* ************************************************************************* */
const removePlayer = (player) => {
  var elPos = document.getElementById(`${player.name}`).parentNode;
  if (!(typeof elPos === 'undefined'))
  {
    elPos.classList.remove('Player');
    elPos.removeChild(elPos.childNodes[0]);
  };
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        POSITION PLAYER Function                           */
/*                                                                           */
/* - - - Initiate player class position                                      */
/* ************************************************************************* */
const posPlayer = (player, posTarget) => {
  var playerElement = document.createElement('div');
  playerElement.id = `${player.name}`;

  if (!(hasClass(isMap[player.pos], 'rocked')))
  {
    posTarget.appendChild(playerElement);
    getMap();
    getPos(player);
    isMap[player.pos].classList.add('Player');
  };
  autoImgPos(player);
  return getPos(player);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        CHECK INIT PLAYER POSITION                         */
/*                                                                           */
/* - - - Check player position                                               */
/* ************************************************************************* */
const checkStartPos = (playerOne, playerTwo) => {
  var randomNumber = Math.floor(Math.random() * 59)+30;

  getPos(playerOne);
  getPos(playerTwo);
  if (!(playerOne.pos === playerTwo.pos))
  {
    if (playerOne.pos > (playerTwo.pos + randomNumber))
    {
      return flur("Game Is Ready");
    }
    else {
      reSetPlayer(playerOne);
      reSetPlayer(playerTwo);
      checkStartPos(playerOne, playerTwo);
      return;
    };
  }
    else {
      reSetPlayer(playerOne);
      reSetPlayer(playerTwo);
      checkStartPos(playerOne, playerTwo);
      return;
    };
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                        Re INIT POSITION PLAYER                            */
/*                                                                           */
/* - - - Reposition Player                                                   */
/* ************************************************************************* */
const reSetPlayer = (player) => {
  getPos(player);
  removePlayer(player);
  setPlayer(player);
  return;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             ACTIVE PLAYER                                 */
/*                                                                           */
/* - - - player is active                                                    */
/* ************************************************************************* */
const getActived = (player) => {
  document.getElementById(player.name).classList.add('active');
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                            SET RANDOM TURN                                */
/*                                                                           */
/* - - - set random Turn                                                     */
/* ************************************************************************* */
const setTurn = (game) => {
  var random = Math.floor((Math.random() * 2) + 1);

  game.turnNumber = random;
  if (random === 1)
    return getActived(playerOne);
  if (random === 2)
    return getActived(playerTwo);
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                           CHANGE TURN Function                            */
/*                                                                           */
/* - - - change turn                                                         */
/* ************************************************************************* */
const changeTurn = (game) => {
  eventCanceller();

  if (game.turnNumber === 1)
  {
    game.turnNumber = 2;
    document.getElementById('playerOne').classList.remove('active');
    getActived(playerTwo);
    return game.count++;
  };

  if (game.turnNumber === 2)
  {
    game.turnNumber = 1
    document.getElementById('playerTwo').classList.remove('active');
    getActived(playerOne);
    return game.count++;
  };
};

//*****************************************************************************
//*****************************************************************************
//********************************  A S S E T S  ******************************
//*****************************************************************************
//*****************************************************************************
//*****************************************************************************

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                             INIT MAP SIZE                                 */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const setTable = () => {
  var table = document.getElementById('map');
    table.style.width = (35 * vd) + "px";
    table.style.height = (35 * vd) + "px";
    table.style.marginTop = ((ch * vd) - (17.5 * vd)) + "px";
    table.style.marginLeft = ((cw * vd) - (17.5 * vd)) + "px";
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                    GET REAL MAP SIZE AFTER TRANSFORM                      */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const isoMapSize = () => {
  var MapSize = document.getElementById('map');

  mapHeight = MapSize.getBoundingClientRect().height;
  mapWidth = MapSize.getBoundingClientRect().width;
  isoInitTop =  ((ch * vd) - ((mapHeight /2) + (1 * vd)));
  isoInitLeft = ((cw * vd) - (1.5 * vd));

  playerSpritePos(playerOne, isoInitTop, isoInitLeft);
  playerSpritePos(playerTwo, isoInitTop, isoInitLeft);
  return[isoInitLeft, isoInitTop, mapWidth, mapHeight];
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                         PLACING PLAYER SPRITE                             */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const playerSpritePos = (player, Top, Left) => {
  var playerImg = document.getElementById(`${player.name}Img`);;
    playerImg.style.width = (3 * vd) +"px";
    playerImg.style.height = (3 * vd) +"px";
    playerImg.style.marginTop = Top + 'px';
    playerImg.style.marginLeft = Left + 'px';
    playerImg.style.zIndex = '4';
  //  playerImg.style.transform = ' rotateX(-45deg)';
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                    POSITION SPRITE TO PLAYER POSITION                     */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const autoImgPos = (player) => {
  var xAxis;
  var yAxis;
  var plyPos;
  var imgPosY;
  var imgPosX;

  if (player.pos <= 9)
  {
    plyPos = '0'+player.pos;
  }
  else {
    plyPos = player.pos;
  }

  xAxis = Number((plyPos).toString()[1]);
  yAxis = Number((plyPos).toString()[0]);
  imgPosY = isoInitTop + ((mapHeight /20) * yAxis) + ((mapWidth /40) * xAxis);
  imgPosX = isoInitLeft - ((mapWidth /20) * yAxis) + ((mapHeight /10) * xAxis);

  var playerImg = document.getElementById(`${player.name}Img`);
      playerImg.style.marginTop = imgPosY + 'px';
      playerImg.style.marginLeft = imgPosX + 'px';
      playerImg.style.zIndex = yAxis + 1;
};

/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                    POSITION SPRITE for Rocked Cells                       */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
const setRockImg = (rock) => {
  var xAxis;
  var yAxis;
  var rockPos;
  var imgPosY;
  var imgPosX;

  if (rock <= 9)
  {
    rockPos = '0'+ rock;
  }
  else {
    rockPos = rock;
  };

  xAxis = Number(rockPos.toString()[1]);
  yAxis = Number(rockPos.toString()[0]);
  imgPosY = isoInitTop + ((mapHeight /20) * yAxis) + ((mapWidth /40) * xAxis);
  imgPosX = isoInitLeft - ((mapWidth /20) * yAxis) + ((mapHeight /10) * xAxis);

  var rockSprite = document.createElement('img');
    rockSprite.style.width = (3 * vd) +"px";
    rockSprite.style.height = (3 * vd) +"px";
    rockSprite.style.marginTop = imgPosY + 5+ 'px';
    rockSprite.style.marginLeft = imgPosX + 'px';
    rockSprite.style.zIndex = yAxis + 1;
    rockSprite.src = "./assets/rocked.svg";
    rockSprite.classList.add('rockedImg');

  return document.querySelector('body').appendChild(rockSprite);
};

const removeRockedSprite = () => {
  var c = document.body.getElementsByClassName('rockedImg');
  var x = c.length;
  let i1 = 0;
  let i2 = 0;

  while (i1 < x)
  {
    c = document.body.getElementsByClassName('rockedImg');
    i2 = 0;
    while (i2 < 5)
    {
      document.body.removeChild(c[i2]);
      if (!(typeof c === 'undefined'))
      {
        return removeRockedSprite();
      };
      i2++;
    };
    i1++;
  };
  return isMap.filter((e, i) => {if (hasClass(e, 'rocked')){setRockImg(i)}});
};

//*****************************************************************************
//*****************************************************************************
//*****************************************************************************
/* ************************************************************************* */
/*                                                                           */
/*                                                                           */
/*                               init game                                   */
/*                                                                           */
/*                                                                           */
/* ************************************************************************* */
getMap();
setRock();
setWeapon();
setPlayer(playerOne);
setPlayer(playerTwo);
checkStartPos(playerOne, playerTwo);
setTurn(game)


/*
do Change weapon
set Img weapon
do Defend
do Notif
do Turn
icone Button
do separate script
Index.Js made of getScript('js/????.js') then function call every initializator;
*/
