//-----------------------------------------------------------------------------
//---------------------- Rubber-Sync Ultra Responsive Value -------------------
//-----------------------------------------------------------------------------
//- Call this Js file before </body> like that <script src="RubberSync.js"></script>
//- - -
//- So "vd" mean "viewport diagonal", here it work in percentage.
//- "vd" is calculated with Pythagore theorem.
//- So "cw" mean "center width" || "ch" in this case it mean "center height".
//- "cw & ch" are calculated with Thales theorem.
//- - -
//- Please Use RubberSync-sheet.Js to write Css for element who will be
//- affected by RubberSync.js
//- - -
//- Example = "For 1600x900 screen || vw=1600px || vh=775px || 1*vd = 17.77px".
//- Horizontal_center = 44.00*vd --- vertical_center = 20.80*vd.
//- - -
//- Don't forget to substract "width & height" of an element when your
//- using ch & cw, for your margin.
//- - -
//- This is an example for how to write a style with Rubber-sync
//- document.getElementById('yourId').style.width = (5 * vd) + "px";
//- document.getElementById('yourId').style.height = (5 * vd) + "px";
//- document.getElementById('yourId').style.marginLeft = ((cw * vd)-(2.5 * vd)) + "px";
//- - -
//- when you set and use "cw & ch" value like that : cw*vd - 'value' *vd
//- Don't forget to like my Facebook page here: https://www.facebook.com/RubberSync/
//- Enjoy !!!!
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//- - -     Declaring Global Var
'use strict';
var vd;
var cw;
var ch;
var xglobal;
var yglobal;

//- - -     Calling Every Function Needed to define Global Var
function rbSync() {
  Gvport();
  Hrn();
	ThsCw();
	ThsCh();
};

//- - -     Getting VH and VW values
function Gvport() {
     if (typeof window.innerWidth != 'undefined') {
       xglobal = window.innerWidth,
       yglobal = window.innerHeight
     }
     else if (typeof document.documentElement != 'undefined'
     && typeof document.documentElement.clientWidth !=
     'undefined' && document.documentElement.clientWidth != 0) {
        xglobal = document.documentElement.clientWidth,
        yglobal = document.documentElement.clientHeight;
     }

     return [xglobal, yglobal];
   };

//- - -    VD with Pythagore
function Hrn () {
		var x = xglobal;
		var y = yglobal;
		var a = Math.sqrt(x * x + y * y);
	     vd = a / 100;
		return (vd);
    };

//- - -  Center with Thales
 function ThsCw () {
		var AB = xglobal;
		var AC = yglobal;
		var CE = AC / 2;
		var EG = CE * AB / AC;
		var Percent = EG / vd;
		    cw = Percent;
		return (cw);
    };

function ThsCh () {
		var AB = yglobal;
		var AC = xglobal;
		var CE = AC / 2;
		var EG = CE * AB / AC;
		var Percent = EG / vd;
        ch = Percent;
		return (ch);
    };

//- - -  Reload Another Js File
function getScript(source, callback) {
    var script = document.createElement('script');
    var prior = document.getElementsByTagName('script')[0];
    script.async = 1;
    prior.parentNode.insertBefore(script, prior);

    script.onload = script.onreadystatechange = function( _, isAbort ) {
        if(isAbort || !script.readyState || /loaded|complete/.test(script.readyState) ) {
            script.onload = script.onreadystatechange = null;
            script = undefined;
            if(!isAbort) { if(callback) callback(); }
        }
    };

        script.src = source;
    }


//-------------------------------------------------------
//------------------------------- auto reload when resize
//-------------------------------------------------------

window.addEventListener('resize', function () {
    "use strict";
    rbSync();
    getScript('js/RubberSync-sheet.js');
  });


  //-------------------------------------------------------
  //--------------------------- I N I T I A L I Z A T I O N
  //-------------------------------------------------------

  rbSync();
  getScript('js/RubberSync-sheet.js');
