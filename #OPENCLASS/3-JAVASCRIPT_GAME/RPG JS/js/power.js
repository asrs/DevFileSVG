/*
///
////
///// G L O B A L  V A R   I N I T
////
///
*/
const cheh  = console.log;
const map   = document.getElementById('map');
const but_attack = document.getElementById('but_attack');
const but_move = document.getElementById('but_move');
const but_defend = document.getElementById('but_defend');

var but_actived = false;

var playerOne = {
name: "playerOne",
vie : 100,
xPos: 0,
yPos: 0,
arme: "Main de fer",
degat: 10,
defend: 0,
tour: 0,
};
var playerTwo = {
name: "playerTwo",
vie : 100,
xPos: 0,
yPos: 0,
arme: "Main de fer",
degat: 10,
defend: 0,
tour: 0,
};
/*
///
////
///// S E T  B U T T O N
////
///
*/
but_attack.addEventListener('click', function () {
  if (but_actived === false)
  {
    but_actived = true;
    attackPartOne(playerOne, playerTwo);
  }
  else {
    but_actived = false;
    eventCanceller();//have to recup eventAction
  };
});

but_move.addEventListener('click', function () {
  if (but_actived === false)
  {
    but_actived = true;
    movePartOne(playerOne);
  }
  else {
    but_actived = false;
    eventCanceller();//have to recup eventAction
  };
});
/*
///
////
///// G E T  C L A S S
////
///
*/
function hasClass(element, cls) {
  return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
};
/*
///
////
///// S E A R C H I N G
////
///
*/
function searchPos (player) {
  var iY = 0;
  var iX = 0;
  var posY = 0;
  var posX = 0;
  var posPlayer = 0;
  var idPlayer = 0;
  var checkCells = 0;

  if (!(idPlayer === player.name)) //- Parsing the map
  {
    while(iY < 10 && idPlayer === 0)
    {
      iX = 0;
      while(iX < 10 && idPlayer === 0)
      {
        checkCells = map.rows[iY].cells[iX];
        posPlayer = checkCells.children.length;
        if (posPlayer > 0)
        {
          idPlayer = checkCells.firstChild.id;
          if (!(idPlayer === player.name))
          {
            idPlayer = 0;
            posPlayer = 0;
          };
        };
        iX++;
      };
      iY++;
    };
  };

  if(idPlayer === player.name) //- Calculating coordonate
  {
    while(posY < iY)
    {
      posY++;
    };
    while(posX < iX)
    {
      posX++
    };
    posY = posY -1;
    posX = posX -1;

    //- Giving coordonate
    player.yPos = posY;
    player.xPos = posX;
    //cheh(map.rows[posY].cells[posX]);
    //cheh(player.yPos+','+ player.xPos);
    return;
  };
};
/*
///
////
///// A T T A C K
//// one = seek cells && active listen / two = action
///
*/
function attackPartOne (player, victim) {
  var victimPos = victim.yPos+','+victim.xPos;
  var playerPos = player.yPos+','+player.xPos;
  var caseOne = map.rows[player.yPos+1];
  var caseTwo = map.rows[player.yPos-1];
  var caseThree = map.rows[player.yPos].cells[player.xPos+1];
  var caseFour = map.rows[player.yPos].cells[player.xPos-1];
  var i = 0;
  var eventAction = [];

  //- Checking if coordonate are in the Map
  if (!(typeof caseOne === 'undefined'))
  {
    caseOne = map.rows[player.yPos+1].cells[player.xPos];
    eventAction.splice(0, 0,caseOne)
  };

  if (!(typeof caseTwo === 'undefined'))
  {
    caseTwo = map.rows[player.yPos-1].cells[player.xPos];
    eventAction.splice(1, 0,caseTwo)
  };

  if (!(typeof caseThree === 'undefined'))
  {
    caseThree = map.rows[player.yPos].cells[player.xPos+1];
    eventAction.splice(2, 0,caseThree)
  };

  if (!(typeof caseFour === 'undefined'))
  {
    caseFour = map.rows[player.yPos].cells[player.xPos-1];
    eventAction.splice(3, 0,caseFour)
  };

  while( i < 4) //Activing cells for Attack
  {
    if (!(typeof eventAction[i] === 'undefined'))
    {
      if (!(hasClass(eventAction[i], 'rocked')))
      {
        eventAction[i].classList.add('attack');
        if (hasClass(eventAction[i], 'attack'))
        {
          eventAction[i].onclick = function handler (e) {
              victimPos = e.target;
              attackPartTwo(player, victim, victimPos, eventAction);
              eventCanceller();
            }, {once: true};
        };
      };
    };
  i++;
  };

  return;
};

function attackPartTwo (player, victim, victimPos, eventAction) {
  var victimCells = map.rows[victim.yPos].cells[victim.xPos];

  if (victimCells === victimPos.parentNode)
  {
    if (victim.defend === 0)
    {
      victim.vie -= player.degat;
    }
    else {
      victim.vie -= (player.degat/2);
    };
    cheh("A T T A C K !");
    cheh(victim.name+" : "+victim.vie+' Pv');
  }
  else {
    cheh("attack missed...")
  };

  return;
};
/*
///
////
///// M O V E M E N T
//// one = seek cells / two = active listen / three = action
///
*/
function movePartOne (player) {
  var playerPos = player.yPos+','+player.xPos;
  var playerWho = map.rows[player.yPos].cells[player.xPos];
  var moveRange = 3;
  var eventAction = [];
  var j = 0;
  var i;

  i = 1;
  while(i < (moveRange+1))
  {
    if (!(typeof map.rows[player.yPos-i] === 'undefined'))
    {
      var moveCells = map.rows[player.yPos-i].cells[player.xPos];
      if (!(hasClass(moveCells, 'rocked')))
      {
        if (moveCells.childNodes.length === 0)
        {
          moveCells.classList.add('move');
          eventAction.push(moveCells);
          j++;
        };
      }
      else {
        i = moveRange+1;
      };
    };
  i++;
  };

  i = 1;
  while(i < (moveRange+1))
  {
    if (!(typeof map.rows[player.yPos+i] === 'undefined'))
    {
      var moveCells = map.rows[player.yPos+i].cells[player.xPos];
      if (!(hasClass(moveCells, 'rocked')))
      {
        if (moveCells.childNodes.length === 0)
        {
        moveCells.classList.add('move');
        eventAction.push(moveCells);
        j++;
        };
      }
      else {
        i = moveRange+1;
      };
    };
  i++;
  };

  i = 1;
  while(i < (moveRange+1))
  {
    if (!(typeof map.rows[player.yPos].cells[player.xPos+i] === 'undefined'))
    {
      var moveCells = map.rows[player.yPos].cells[player.xPos+i];
      if (!(hasClass(moveCells, 'rocked')))
      {
        if (moveCells.childNodes.length === 0)
        {
        moveCells.classList.add('move');
        eventAction.push(moveCells);
        j++;
        };
      }
      else {
        i = moveRange+1;
      };
    };
  i++;
  };

  i = 1;
  while(i < (moveRange+1))
  {
    if (!(typeof map.rows[player.yPos].cells[player.xPos-i] === 'undefined'))
    {
      var moveCells = map.rows[player.yPos].cells[player.xPos-i];
      if (!(hasClass(moveCells, 'rocked')))
      {
        if (moveCells.childNodes.length === 0)
        {
        moveCells.classList.add('move');
        eventAction.push(moveCells);
        j++;
        };
      }
      else {
        i = moveRange+1;
      };
    };
  i++;
  };

movePartTwo(player, playerWho, eventAction, j);
return;
};

function movePartTwo (player, playerWho, eventAction, count) {
  var i = 0;

  while(i < count)
  {
    eventAction[i].onclick = function handler (e) {
      if (hasClass(e.target, 'move'))
      {
        if (!(e.target.hasChildNodes()))
        {
          playerWho = map.rows[player.yPos].cells[player.xPos];
          e.target.appendChild(playerWho.childNodes[0]);
          if (player.name === 'playerOne')
          {
            searchPos(playerOne);
            playerWho = map.rows[player.yPos].cells[player.xPos];
          }
          else {
            searchPos(playerTwo);
            playerWho = map.rows[player.yPos].cells[player.xPos];
          };
        };
      };
      //this.removeEventListener('click', handler);
      movePartThree (player, playerWho, eventAction, count);
      eventCanceller();
    };
    i++;
  };
  return;
};

function movePartThree (player, playerWho, eventAction, count) {
  var i = 0;

  while (i < count)
  {
    if (eventAction[i].hasChildNodes())
    {
      var playerElement = document.createElement('div');
      playerElement.id = "playerOne";
      playerElement.textContent = "01";

      eventAction[i].removeChild(eventAction[i].firstChild);
      map.rows[player.yPos].cells[player.xPos].appendChild(playerElement);
    };
    i++;
  };
  return;
};
/*
///
////
///// E V E N T  C A N C E L L E R
////
///
*/
function eventCanceller () {
  var checkCells;
  var iY = 0;
  var iX = 0;

  while(iY < 10)
  {
    iX = 0;
    while(iX < 10)
    {
      checkCells = map.rows[iY].cells[iX];

      if (!(hasClass(checkCells,'rocked')))
      {
        if (hasClass(checkCells,'attack'))
        {
          if (!(typeof checkCells === 'undefined'))
          {
            //cheh('attack cells find');
            checkCells.onclick = "null";
            checkCells.classList.remove('attack');
          };
        };

        if (hasClass(checkCells,'move'))
        {
          if (!(typeof checkCells === 'undefined'))
          {
            //cheh('move cells find');
            checkCells.onclick = "null";
            checkCells.classList.remove('move');
          };
        };
      };
      iX++;
    };
      iY++;
  };
  return;
};
/*
///
////
///// I N I T   R O C K
////
///
*/
function setRock () {
  var randomNumber = Math.floor((Math.random() * 20) + 1)
  var i = 0;

  while(i < randomNumber)
  {
    var randomY = Math.floor(Math.random() * 10);
    var randomX = Math.floor(Math.random() * 10);

    // CREATE IMG ELEMENT APPENDCHILD('body') for ROCK ?

    if (hasClass(map.rows[randomY].cells[randomX], 'rocked'))
    {
      randomY = Math.floor(Math.random() * 10);
      randomX = Math.floor(Math.random() * 10);
      i--;
    }
    else {
     map.rows[randomY].cells[randomX].classList.add('rocked');
    };
    i++;
  };
  return;
};
/*
///
////
///// I N I T   P L A Y E R S
////
///
*/
function setPlayerOne () {
  var randomY = Math.floor(Math.random() * 10);
  var randomX = Math.floor(Math.random() * 10);
  var playerElement = document.createElement('div');
  playerElement.id = "playerOne";
  //playerElement.textContent = "01";

  if (hasClass(map.rows[randomY].cells[randomX], 'rocked'))
  {
    setPlayerOne();
  }
  else {
    map.rows[randomY].cells[randomX].appendChild(playerElement);
    searchPos(playerOne);
    return;
  };
};

function setPlayerTwo () {
  var randomY = Math.floor(Math.random() * 10);
  var randomX = Math.floor(Math.random() * 10);
  var playerElement = document.createElement('div');
  playerElement.id = "playerTwo";
  //playerElement.textContent = "02";

  if (hasClass(map.rows[randomY].cells[randomX], 'rocked'))
  {
    setPlayerTwo();
  }
  else {
    map.rows[randomY].cells[randomX].appendChild(playerElement);
    searchPos(playerTwo);
    return;
  };
};

function checkStartPos () {

  if (playerTwo.yPos >= (playerOne.yPos+4) || playerTwo.yPos <= (playerOne.yPos-4))
  {
    if (playerTwo.xPos <= (playerOne.xPos+3) || playerTwo.xPos >= (playerOne.xPos-3))
    {
      cheh("Game Is Ready");
    }
    else {
      searchPos(playerOne);
      searchPos(playerTwo);
      removePlayer(playerOne);
      removePlayer(playerTwo);
      setPlayerOne();
      setPlayerTwo();
      checkStartPos();
    };
  }
  else {
    searchPos(playerOne);
    searchPos(playerTwo);
    removePlayer(playerOne);
    removePlayer(playerTwo);
    setPlayerOne();
    setPlayerTwo();
    checkStartPos();
  };
};

function removePlayer(player) {
  elPos = document.getElementById(player.name);
  PosPlayer = map.rows[player.yPos].cells[player.xPos];
  isExist = PosPlayer.length;

  if (!(typeof elPos === 'undefined'))
  {
    if (!(isExist === 0 ) || !(typeof isExist === 'undefined'))
    {
        map.rows[player.yPos].cells[player.xPos].removeChild(elPos);
    };
  };
};
/*
///
////
///// T U R N   M E C A N I C S
////
///
*/
function setTurn () {
  var random = Math.floor((Math.random() * 2) + 1);

  if (random === 1)
  {
    getActived(playerOne);
  };
  if (random === 2)
  {
    getActived(playerTwo);
  };
};

function turnTimer () {
/*setTimeout get active 1 if two was active et vice versa*/
};

function getActived (player) {
  player.tour = 1;
  document.getElementById(player.name).classList.add('active');
};

function getUnactived (player) {
  player.tour = 0;
  document.getElementById(player.name).classList.remove('active');
};
/*
///
////
/////
////
///
*/

/*
///
////
/////
////
///
*/

//====================================================
//
//- - - DEEP
//
//Fonction pour activé la défense
//Fonction de gestion du tour par tour
//Fonction de distribution des armes
//Fonction de ramassage des armes
//
//- - - UI
//
//Notification: Changement de Tour, Prise de Dégat <--- Affichage
//Timer
//
//and for Isometric view.
//
//
//- - - FINAL
//
//  Separation function
//  Synthetisation with 'const x = (arg1, arg2...etc) => Action '
//
//====================================================

setRock();
setPlayerOne();
setPlayerTwo();
checkStartPos();
setTurn();
//movePartOne(playerOne);
//attackPart1(playerOne, playerTwo);
