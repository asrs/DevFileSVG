/*
Activité : gestion des contacts
*/

//----------------------------------------------------------------------- Declare global var
var optionUI = ["2 - Ajouter un contact", "1 - Lister les contacts", "0 - Quitter le programme"];
var choixUI;
var contactList = [];

//----------------------------------------------------------------------- Premier Contacts
var contactNb = {

  init: function (nom, prenom) {
     this.nom = nom;
     this.prenom = prenom;
                                },

  decrire: function() {
    var description ="Nom: "+ this.nom +" Prénom: "+ this.prenom;
    return description;
                      }
                }

var contact1 = Object.create(contactNb);
contact1.init("Adama", "Capitaine");

var contact2 = Object.create(contactNb);
contact2.init("Snow", "John");

contactList.push(contact1);
contactList.push(contact2);
var contactLength = contactList.length;

//----------------------------------------------------------------------- FUNCTION
function indicationMenu () {
  console.log(' ')
  console.log(optionUI[0])
  console.log(optionUI[1])
  console.log(optionUI[2])
  choixUI = prompt("Entrée le chiffre correspondant au menu", " ");
  return (choixUI);
    }

function action(){

    if (choixUI === '2') { //---------------------------AJOUTER LE CONTACT
      var inputNom = prompt("Nom du contact ?", " ");
      var inputPrenom = prompt("Prénom du contact ?", " ");

      var newContact = Object.create(contactNb);
      newContact.init(inputNom, inputPrenom);

      contactList.push(newContact);
      var contactLength = contactList.length;
      console.log("-------------- Le nouveau contact à étais ajouté !")
      indicationMenu();
      action();
    }

    else if (choixUI === '1') {//---------------------LISTER LE CONTACT
      // ...
      console.log(' ');
      console.log('------------------- Liste de Contacts')
      contactList.forEach(function (contactNb) {//-- merci open class room ^^
          console.log(contactNb.decrire());
      });
      console.log('------------------- ');
      indicationMenu();
      action();
    }

    else if (choixUI === '0') { //--------------------QUITTER LE PROGRAMME
      console.log("Merci beaucoup et à bientôt !")
    }
    else if (choixUI >= 3 || choixUI <= -1){//-------------Wrong Number Entered
    console.log("Action inconnu, référé vous au menu !")
    indicationMenu();
    action();
    }


}
//----------------------------------------------------------------------- PROCESS
console.log("Bonjour, bienvenu dans le gestionnaire de contacts, choisissez une option :")
indicationMenu();
action();
