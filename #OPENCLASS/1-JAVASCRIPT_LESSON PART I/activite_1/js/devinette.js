/*
Activité : jeu de devinette
*/

// NE PAS MODIFIER OU SUPPRIMER LES LIGNES CI-DESSOUS
// COMPLETEZ LE PROGRAMME UNIQUEMENT APRES LE TODO

console.log("Bienvenue dans ce jeu de devinette !");

// Cette ligne génère aléatoirement un nombre entre 1 et 100
var solution = Math.floor(Math.random() * 100) + 1;

// Décommentez temporairement cette ligne pour mieux vérifier le programme
//console.log("(La solution est " + solution + ")");

// TODO : complétez le programme

var essaie = 1
var prop

while (prop !== solution && essaie <= 6)
{
 prop = Number(prompt("Entrez un nombre :"));

  if (prop > solution){
    console.log(`${prop} est trop grand`)
    essaie++
  }

  if (prop < solution){
    console.log(`${prop} est trop petit`)
    essaie++
  }
}

if (prop === solution){console.log(`Bravo vous avez trouver le chiffe ${solution},Vous avez gagné en ${essaie} essaie !`)}
if (essaie > 6){console.log("Vous avez dépassé le nombre d'essaie autorisé, c'est perdu !")}
