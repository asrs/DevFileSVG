//-------------------------------------------------------- DECLARATION
var motChoisi = "GRENADE";
var motLength = motChoisi.length;
var motDuree = (motLength - 1);
var motReverse = 0;
var nbVoyelle = 0;
var pointeur1 = 0;
var pointeur2 = motChoisi.length-1;
var reversedWord = '';
var leetChoisi = motChoisi;

//-------------------------------------------------------- FUNCTION
function voyelle(){
    var voyelleChoisi = motChoisi[pointeur1];
        if ( voyelleChoisi === "a" || voyelleChoisi === "e" ||
             voyelleChoisi === "i" || voyelleChoisi === "o" ||
             voyelleChoisi === "u")
        {
          nbVoyelle++;
          return(nbVoyelle);
        }
        if ( voyelleChoisi === "A" || voyelleChoisi === "E" ||
             voyelleChoisi === "I" || voyelleChoisi === "O" ||
             voyelleChoisi === "U")
        {
         nbVoyelle++;
         return(nbVoyelle);
        }
    }

function reverse(){
    reversedWord +=motChoisi[pointeur2];
    return (reversedWord);
    }

function Leetspeak(){
  var voyelleChoisi = motChoisi[pointeur1];

    if ( voyelleChoisi === "A" || voyelleChoisi === "a")
    {
    leetChoisi = leetChoisi.replace(voyelleChoisi,"4");
    }

    if ( voyelleChoisi === "E" || voyelleChoisi === "e")
    {
    leetChoisi = leetChoisi.replace(voyelleChoisi,"3");
    }

    }
//-------------------------------------------------------- LOOP

while( pointeur1 <= motDuree )
{
voyelle();
Leetspeak();
pointeur1++
}

while( pointeur2 >= 0)
{
reverse();
pointeur2--
}

//-------------------------------------------------------- RESULTAT

console.log(`le mot ${motChoisi} contient ${motLength} lettre.`)
console.log(`Il contient ${nbVoyelle} voyelle.`)
console.log(`Le mot à l'envers s'écrit ${reversedWord}`)
console.log(`Le mot en Leetspeak s'écrit ${leetChoisi}`)
