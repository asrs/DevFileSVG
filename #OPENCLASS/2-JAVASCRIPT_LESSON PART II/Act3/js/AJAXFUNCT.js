function ajaxGet (url, callback) {
  const req = new XMLHttpRequest();

  req.open("GET",url, true);
  req.addEventListener("load", function () {
      if (req.status >= 200 && req.status < 400)
      {
        callback(req.responseText);
      }
      else
      {
        console.log(req.status + " " + req.statusText + " " + url);
      }
  });
  req.addEventListener("error", function () {
    console.log("Network Error " + url);
  });
  req.send(null);
};

function ajaxPost(url, data, isJson, callback) {
    const req = new XMLHttpRequest();

    req.open("POST", url);
    req.addEventListener("load", function () {
        if (req.status >= 200 && req.status < 400) {
            // Appelle la fonction callback en lui passant la réponse de la requête
            callback(req.responseText);
        } else {
            console.error(req.status + " " + req.statusText + " " + url);
        }
    });
    req.addEventListener("error", function () {
        console.log("Network Error " + url);
    });
    if (isJson) {
    // Définit le contenu de la requête comme étant du JSON
    req.setRequestHeader("Content-Type", "application/json");
    // Transforme la donnée du format JSON vers le format texte avant l'envoi
    };
    req.send(data);
};
