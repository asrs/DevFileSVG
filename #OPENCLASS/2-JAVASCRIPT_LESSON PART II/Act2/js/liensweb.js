//  Salut à toi Noble Correcteur !
//  J'avais à la base utilisés des fonctions pour contenir les actions
//  de l'event listener, mais finalement j'ai fait comme ça;
//  donne moi ton avis si tu aurais préféré que je garde les function !!
//  Merci ;)

//================================================= Global Declaration !
const contenu = document.getElementById("contenu");
var formMod = 0;

//================================================= Existing Object !
var listeLiens = [
    {
        titre: "So Foot",
        url: "http://sofoot.com",
        auteur: "yann.usaille"
    },
    {
        titre: "Guide d'autodéfense numérique",
        url: "http://guide.boum.org",
        auteur: "paulochon"
    },
    {
        titre: "L'encyclopédie en ligne Wikipedia",
        url: "http://Wikipedia.org",
        auteur: "annie.zette"
    }
  ];

//================================================= Create Element Function
function creerElementLien(arrayLien) {
    var titreLien = document.createElement("a");
    titreLien.href = arrayLien.url;
    titreLien.style.color = "#428bca";
    titreLien.style.textDecoration = "none";
    titreLien.style.marginRight = "5px";
    titreLien.appendChild(document.createTextNode(arrayLien.titre));

    var urlLien = document.createElement("span");
    urlLien.appendChild(document.createTextNode(arrayLien.url));

    // Cette ligne contient le titre et l'URL du lien
    var ligneTitre = document.createElement("h4");
    ligneTitre.style.margin = "0px";
    ligneTitre.appendChild(titreLien);
    ligneTitre.appendChild(urlLien);

    // Cette ligne contient l'auteur
    var ligneDetails = document.createElement("span");
    ligneDetails.appendChild(document.createTextNode("Ajouté par " + arrayLien.auteur));

    var divLien = document.createElement("div");
    divLien.classList.add("lien");
    divLien.appendChild(ligneTitre);
    divLien.appendChild(ligneDetails);

    return divLien;
}

//=============================================== Add Existant Element
listeLiens.forEach(function (obj) {
    var elementLien = creerElementLien(obj);
    contenu.appendChild(elementLien);
  });

//=============================================== Create Formulaire in DOM !
  var formButton = document.createElement("button");
  formButton.type = "submit";
  formButton.textContent = "Ajouter un lien";

  var formContainer =  document.createElement("form");
  formContainer.id = "formElt";
  formContainer.onsubmit = "return false";
  formContainer.style.paddingBottom = "15px";
  formContainer.appendChild(formButton);

  contenu.insertBefore(formContainer, contenu.childNodes[0]);

//=============================================== ADD NEW LINK EVENT && ACTION
document.getElementById('formElt').addEventListener("submit", function (e) {
  if (formMod === 0)
    {
      formMod = 1;

      //=== modif Button
      formButton.textContent = "Ajouter";
      formButton.style.marginLeft = "8px";

      //=== create Input Element
      var urlIn = document.createElement("input");
      urlIn.id = "inputUrl";
      urlIn.required = true;
      urlIn.type = "text";
      urlIn.placeholder = "Entrez l'URL du lien";
      urlIn.size = "50";
      urlIn.style.marginLeft = "8px";
      urlIn.style.float = "left";

      var titreIn = document.createElement("input");
      titreIn.id = "inputTitre"
      titreIn.required = true;
      titreIn.type = "text";
      titreIn.placeholder = "Entrez le titre du lien";
      titreIn.size = "30";
      titreIn.style.marginLeft = "8px";
      titreIn.style.float = "left";

      var auteurIn = document.createElement("input");
      auteurIn.id = "inputAuteur"
      auteurIn.required = true;
      auteurIn.type = "text";
      auteurIn.placeholder = "Entrez votre nom";
      auteurIn.style.float = "left";

      formContainer.insertBefore(auteurIn, formButton);
      formContainer.insertBefore(titreIn, formButton);
      formContainer.insertBefore(urlIn, formButton);
    }
    else if (formMod === 1)
      {
        formMod = 0;

        //=== modif Button
        formButton.textContent = "Ajouter un lien";
        formButton.style.marginLeft = "0px";

        //=== Declaring form Element
        var auteurElt = document.getElementById("inputAuteur");
        var titreElt = document.getElementById("inputTitre");
        var urlElt = document.getElementById("inputUrl")

        //=== Check and Add HTTP://
        var regexUrl1 = /http\:\/\/.+/;
        var regexUrl2 = /https\:\/\/.+/;

        if (regexUrl1.test(urlElt.value) === false && regexUrl2.test(urlElt.value) === false)
          {
            var httpAdder =`http://${urlElt.value}`
            urlElt.value = httpAdder;
              //console.log(urlElt.value)
          };

        //=== Create Object and MAKE IT REALLLLL
        var newLien = {
            titre: titreElt.value,
            url: urlElt.value,
            auteur: auteurElt.value
          };

        var elementLien = creerElementLien(newLien);
        contenu.insertBefore(elementLien, contenu.childNodes[1]);

        //=== Adding a 2 seconds message
        var indicContent = document.createElement("p");
        indicContent.textContent = `Le Lien "${titreElt.value}" à correctement était ajouté !`;
        indicContent.style.textColor = "blue"
        indicContent.style.fontSize = "large";
        indicContent.style.textAlign = "center";
        indicContent.style.padding = "10px";
        indicContent.style.borderRadius = "5px";
        indicContent.style.backgroundColor = "lightyellow";
        //indicElt.style.

        var indicDiv = document.createElement("div");
        indicDiv.id = "indDiv";
        indicDiv.style.paddingBottom = "60 px";
        indicDiv.appendChild(indicContent);

        contenu.insertBefore(indicDiv, contenu.childNodes[0]);

        var indicElt = document.getElementById('indDiv');

        setTimeout(function(){contenu.removeChild(indicElt);}, 2000);

        //=== delete Input Element
        formContainer.removeChild(auteurElt);
        formContainer.removeChild(titreElt);
        formContainer.removeChild(urlElt);
      };

  e.preventDefault();
});
