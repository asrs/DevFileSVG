const   formElt = document.querySelector('form');
const   nameElt = document.getElementById('Pseudo');
const   choixElt = document.getElementsByName('ocool');
const   thinkElt = document.getElementById('think');

var info = {pseudo:"", evaluation:"", message:""};

nameElt.addEventListener("blur", function () {
    var nameInf = nameElt.value;
    info.pseudo = nameInf;
  });

var i = 0;
while (i < choixElt.length)
{
  choixElt[i].addEventListener("change", function (e) {
    var choixInf = e.target.value;
    info.evaluation = e.target.value;
    });
  i++;
};

thinkElt.addEventListener("blur", function () {
      var thinkInf = thinkElt.value;
      info.message = thinkInf;
    });

formElt.addEventListener('submit', function (e) {
  var dataInf = JSON.stringify(info);
  console.log(dataInf);

  ajaxPost("http://oc-jswebsrv.herokuapp.com/api/temoignage",dataInf, true, function () {
    console.log("Sended :D")
    });

  e.preventDefault();
  });
