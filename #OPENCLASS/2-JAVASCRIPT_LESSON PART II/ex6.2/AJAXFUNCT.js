function ajaxGet (url, callback) {
  const req = new XMLHttpRequest();

  req.open("GET",url, true);
  req.addEventListener("load", function () {
    if (req.status >= 200 && req.status < 400)
    {
      callback(req.responseText);
    }
    else
    {
      console.log(req.status + " " + req.statusText + " " + url);
    }
  });
  req.addEventListener("error", function () {
    console.log("Network Error");
  });
  req.send(null);
};
