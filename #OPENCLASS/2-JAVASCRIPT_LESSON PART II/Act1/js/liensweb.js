/*
Activité 1
*/

// Liste des liens Web à afficher. Un lien est défini par :
// - son titre
// - son URL
// - son auteur (la personne qui l'a publié)
var listeLiens = [
    {
        titre: "So Foot",
        url: "http://sofoot.com",
        auteur: "yann.usaille"
    },
    {
        titre: "Guide d'autodéfense numérique",
        url: "http://guide.boum.org",
        auteur: "paulochon"
    },
    {
        titre: "L'encyclopédie en ligne Wikipedia",
        url: "http://Wikipedia.org",
        auteur: "annie.zette"
    }
];

// TODO : compléter ce fichier pour ajouter les liens à la page web
//--------------------------------------------------------------------------------------------------------------


var saveDiv =[];

  function builder()//Create && Define
    {
      var i = 0;

      while (i <= 2)
        {
          var Creator = `<div class="divContain"><p><a href="${listeLiens[i].url}">${listeLiens[i].titre}</a>
           ${listeLiens[i].url}<br />Ajouté par ${listeLiens[i].auteur}</p></div> `

          saveDiv.push(Creator);
          i++
        }
    }

  function adder()//a add element in DOM
    {
      var i = 0;

      while (i <= 2)
        {
          document.getElementById("contenu").innerHTML += saveDiv[i];
          i++
        }
    }

  function painter()//Change CSS property
    {
      var divsElts = document.getElementsByClassName("divContain");
      var pElts = document.getElementsByTagName('p');
      var aElts = document.getElementsByTagName('a');

      for (var i = 0; i < divsElts.length; i++)
      {
        divsElts[i].style.backgroundColor = "white";
        divsElts[i].style.height ="70px";
      }

      for (var i = 0; i < pElts.length; i++)
      {
        pElts[i].style.padding = "10px";
        pElts[i].style.lineHeight = "160%";
      }

      for (var i = 0; i < aElts.length; i++)
      {
        aElts[i].style.paddingRight ="5px";
        aElts[i].style.fontSize ="x-large";
        aElts[i].style.fontWeight ="bold";
        aElts[i].style.color ="#428bca";
        aElts[i].style.textDecoration ="none";
      }

    }

builder()
adder()
painter()
